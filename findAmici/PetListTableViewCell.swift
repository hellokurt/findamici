//
//  PetListTableViewCell.swift
//  PetSociety
//
//  Created by Diego Riccardi on 26/05/2020.
//  Copyright © 2020 Diego Riccardi. All rights reserved.
//

import UIKit

class PetListTableViewCell: UITableViewCell {
    
    

    
    
    @IBOutlet weak var petPic: UIImageView!
    
    @IBOutlet weak var petGender: UIImageView!
    
    @IBOutlet weak var petName: UILabel!
    
    @IBOutlet weak var petBreed: UILabel!
    
    @IBOutlet weak var petAge: UILabel!
    
    @IBOutlet weak var localita: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.layer.cornerRadius = 10
        self.layer.backgroundColor = UIColor.clear.cgColor
        petPic.layer.cornerRadius = 10
//        vieww.layer.cornerRadius = 10
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    
    
    
    
}




