//
//  FiltersCollectionViewCell.swift
//  PierDiego
//
//  Created by Diego Riccardi on 07/05/2020.
//  Copyright © 2020 Diego Riccardi. All rights reserved.
//

import UIKit

class FiltersCollectionViewCell: UICollectionViewCell {
    
    
    @IBOutlet weak var filterImage: UIImageView!
    
    @IBOutlet weak var filterLabel: UILabel!
    
    

    
    
    override func awakeFromNib() {
        
        self.layer.cornerRadius = 20
        self.layer.borderWidth = 1
        self.layer.masksToBounds = true
        self.layer.borderColor = UIColor.white.cgColor
        filterImage.contentMode = .scaleAspectFit
//        self.filterImage.frame.size = CGSize(width: filterImage.frame.size.width / 100 , height: filterImage.frame.size.height / 100)
        self.filterLabel.textColor = .white
        self.filterImage.tintColor = .white
     
    
        
    }
    
    
//    func toggleSelectedState(){
//        if isSelected == true {
//            backgroundColor = UIColor.white
//            filterLabel.textColor = UIColor.clear
//            filterImage.tintColor = UIColor.clear
//        } else {
//            backgroundColor = UIColor.clear
//            filterLabel.textColor = UIColor.white
//            filterImage.tintColor = UIColor.white
//        }
//
//    }
    
    
    
//    override var isSelected: Bool{
//      didSet{
//        if self.isSelected == true
//        {
//            self.contentView.backgroundColor = UIColor.white
//            self.filterLabel.textColor = .orange
//            self.filterImage.image = UIImage(named: "Vector22")
//        }
//            else if self.isSelected == false
//        {
//            self.contentView.backgroundColor = UIColor.clear
//            self.filterLabel.textColor = .white
//        }
//      }
//    }
    
    
    func showIcon(sendr: FiltersCollectionViewCell){
        if sendr.filterImage.image == UIImage(named: "Vector2") {
            self.contentView.backgroundColor = UIColor.white
            self.filterLabel.textColor = .orange
            self.filterImage.image = UIImage(named: "Vector22")
            
        } else if sendr.filterImage.image == UIImage(named: "Vector3"){
            self.contentView.backgroundColor = UIColor.white
            self.filterLabel.textColor = .orange
            self.filterImage.image = UIImage(named: "Vector33")
            
        } else if sendr.filterImage.image == UIImage(named: "Vector4"){
           self.contentView.backgroundColor = UIColor.white
           self.filterLabel.textColor = .orange
           self.filterImage.image = UIImage(named: "Vector44")
            
        } else if sendr.filterImage.image == UIImage(named: "Vector5"){
           self.contentView.backgroundColor = UIColor.white
           self.filterLabel.textColor = .orange
           self.filterImage.image = UIImage(named: "Vector55")
            
        } else if sendr.filterImage.image == UIImage(named: "Vector6"){
           self.contentView.backgroundColor = UIColor.white
           self.filterLabel.textColor = .orange
           self.filterImage.image = UIImage(named: "Vector66")
            
        }else if sendr.filterImage.image == UIImage(named: "Vector1"){
            self.contentView.backgroundColor = UIColor.white
            self.filterLabel.textColor = .orange
            self.filterImage.image = UIImage(named: "Vector11")
        }

    }
    
    
    
    func hideIcon(sendr: FiltersCollectionViewCell) {
        if sendr.filterImage.image == UIImage(named: "Vector22"){
            self.contentView.backgroundColor = nil
            self.filterLabel.textColor = .white
            self.filterImage.image = UIImage(named: "Vector2")
        } else if sendr.filterImage.image == UIImage(named: "Vector33"){
            self.contentView.backgroundColor = nil
            self.filterLabel.textColor = .white
            self.filterImage.image = UIImage(named: "Vector3")
        } else if sendr.filterImage.image == UIImage(named: "Vector44"){
            self.contentView.backgroundColor = nil
            self.filterLabel.textColor = .white
            self.filterImage.image = UIImage(named: "Vector4")
        } else if sendr.filterImage.image == UIImage(named: "Vector55"){
            self.contentView.backgroundColor = nil
            self.filterLabel.textColor = .white
            self.filterImage.image = UIImage(named: "Vector5")
        } else if sendr.filterImage.image == UIImage(named: "Vector66"){
            self.contentView.backgroundColor = nil
            self.filterLabel.textColor = .white
            self.filterImage.image = UIImage(named: "Vector6")
        } else if sendr.filterImage.image == UIImage(named: "Vector11"){
            self.contentView.backgroundColor = nil
            self.filterLabel.textColor = .white
            self.filterImage.image = UIImage(named: "Vector1")
        }
        
    }
    
    
    
    
    
    
}
