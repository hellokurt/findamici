//
//  FirstViewController.swift
//  PierDiego
//
//  Created by Diego Riccardi on 26/05/2020.
//  Copyright © 2020 Diego Riccardi. All rights reserved.
//

import UIKit

class FirstViewController: UIViewController {
    
    var imagesArray : [UIImage] = [#imageLiteral(resourceName: "crown1"),#imageLiteral(resourceName: "crown2")]

    @IBOutlet weak var crownImageView: UIImageView!
    
    @IBOutlet weak var mainVCButton: UIButton!
    
    @IBOutlet weak var registerButton: UIButton!
    
    @IBAction func mainVCTapped(_ sender: UIButton) {
        self.performSegue(withIdentifier: "segueToMain", sender: UIButton.self)
        
        
        
    }
    
    
   override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        if let nextViewController = segue.destination as? ViewController {
            nextViewController.modalPresentationStyle = .fullScreen
        }

    }
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor(patternImage: #imageLiteral(resourceName: "orange"))
        
        flipCrowns(imageView: crownImageView, images: imagesArray)
        
        mainVCButton.layer.cornerRadius = 23
        mainVCButton.layer.borderWidth = 2
        mainVCButton.layer.borderColor = UIColor.white.cgColor
        
        registerButton.layer.cornerRadius = 23
        registerButton.layer.borderWidth = 2
        registerButton.layer.borderColor = UIColor.white.cgColor
        
        if crownImageView.image == nil {
            crownImageView.image = UIImage(named: "Crown2")
        }
    
        
    }
    

   
    
    
    func flipCrowns (imageView: UIImageView, images: [UIImage]){
        imageView.animationImages = images
        imageView.animationDuration = 2.0
        imageView.animationRepeatCount = 60
        imageView.startAnimating()
    }
    
    
    
    

}
