//
//  FiltersVC.swift
//  PierDiego
//
//  Created by Diego Riccardi on 18/05/2020.
//  Copyright © 2020 Diego Riccardi. All rights reserved.
//

import UIKit

class FiltersVC: UIViewController {
    
//    **BUTTONS**IMAGES**
    
    
    @IBOutlet weak var dogImage: UIImageView!
  
    @IBOutlet weak var dogButton: UIButton!
    
    @IBOutlet weak var dogView: UIView!
    
    @IBOutlet weak var catButton: UIButton!
    
    @IBOutlet weak var catImage: UIImageView!
    
    @IBOutlet weak var catView: UIView!
    
    @IBOutlet weak var heartButton: UIButton!
    
    @IBOutlet weak var heartImage: UIImageView!
    
    @IBOutlet weak var heartView: UIView!
    
    @IBOutlet weak var MaleButton: UIButton!
    
    @IBOutlet weak var maleImage: UIImageView!
    
    @IBOutlet weak var maleView: UIView!
    
    @IBOutlet weak var femaleButton: UIButton!
    
    @IBOutlet weak var femaleImage: UIImageView!
    
    @IBOutlet weak var femaleView: UIView!
    
//    @IBOutlet weak var first: UIImageView!
//
//    @IBOutlet weak var second: UIImageView!
//
//    @IBOutlet weak var third: UIImageView!
//
    
    
    
    //    **SLIDERS**

    @IBOutlet weak var distanceSlider: UISlider!
    
    @IBOutlet weak var distanceImg: UIImageView!
    
    @IBOutlet weak var distanceValue: UILabel!
    
    @IBOutlet weak var ageSlider: UISlider!
    
    
    @IBAction func changeTip(_ sender: UISlider) {
        sizeSlider.value = roundf(sizeSlider.value)
    }
    
    @IBOutlet weak var sizeSlider: UISlider!
    
    @IBOutlet weak var sizeImg: UIImageView!
    
    @IBOutlet weak var sizeValue: UILabel!
    
    //    **VARIABLES**
    
    var valoreboolDog : Bool = false
    
    var valoreboolCat : Bool = false
    
    var valoreboolHeart : Bool = false
    
    var valoreboolMale: Bool = false
    
    var valoreboolFemale : Bool = false
    
//    var valoreboolDog : Bool = false
//
//    var valoreboolDog : Bool = false

    
    

    //    **ACTIONS**
    
    @IBAction func dogTap(_ sender: UIButton) {
        activateDogButton(image: dogImage, view: dogView, butn: dogButton)
       
        
    }
    
    @IBAction func catTap(_ sender: UIButton) {
        activateCatButton(image: catImage, view: catView, butn: catButton)
        
    }
    
    @IBAction func eartTap(_ sender: Any) {
        activateHeartButton(image: heartImage, view: heartView, butn: heartButton)
    }
    
    @IBOutlet weak var applyView: UIView!
    
    @IBOutlet weak var shelterView: UIView!
    
    @IBOutlet weak var privateView: UIView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        shelterView.layer.cornerRadius = 18
        shelterView.layer.borderWidth = 2
       shelterView.layer.masksToBounds = true
        shelterView.layer.borderColor = UIColor.white.cgColor
        applyView.layer.borderColor = UIColor.white.cgColor
        applyView.layer.borderWidth = 2
        applyView.layer.cornerRadius = 18
        
        
        privateView.layer.cornerRadius = 18
        privateView.layer.borderWidth = 2
       privateView.layer.masksToBounds = true
        privateView.layer.borderColor = UIColor.white.cgColor
        
//
//        first.contentMode = .scaleAspectFit
//        second.contentMode = .scaleAspectFit
//        third.contentMode = .scaleAspectFit
        
        dogImage.contentMode = .scaleAspectFit
        dogView.layer.cornerRadius = 18
        dogView.layer.borderWidth = 2
        dogView.layer.masksToBounds = true
        dogView.layer.borderColor = UIColor.white.cgColor
        
        catImage.contentMode = .scaleAspectFit
        catView.layer.cornerRadius = 18
        catView.layer.borderWidth = 2
        catView.layer.masksToBounds = true
        catView.layer.borderColor = UIColor.white.cgColor

        heartImage.contentMode = .scaleAspectFit
        heartView.layer.cornerRadius = 18
        heartView.layer.borderWidth = 2
        heartView.layer.masksToBounds = true
        heartView.layer.borderColor = UIColor.white.cgColor
        
        maleImage.contentMode = .scaleAspectFit
        maleView.layer.cornerRadius = 18
        maleView.layer.borderWidth = 2
        maleView.layer.masksToBounds = true
        maleView.layer.borderColor = UIColor.white.cgColor
        
        femaleImage.contentMode = .scaleAspectFit
        femaleView.layer.cornerRadius = 18
        femaleView.layer.borderWidth = 2
        femaleView.layer.masksToBounds = true
        femaleView.layer.borderColor = UIColor.white.cgColor
        
        
        
    
        distanceImg.contentMode = .scaleAspectFit
        sizeImg.contentMode = .scaleAspectFit

        self.view.backgroundColor = UIColor(patternImage: #imageLiteral(resourceName: "orange"))
        
      
        sizeSlider.setThumbImage(UIImage(named: "Ellipse 2"), for: .normal)
        sizeSlider.addTarget(self, action: #selector(sizeDidChange(slider:)), for: .valueChanged)
        sizeDidChange(slider: sizeSlider)
        
        ageSlider.setThumbImage(UIImage(named: "Ellipse 2"), for: .normal)
        distanceSlider.setThumbImage(UIImage(named: "Ellipse 2"), for: .normal)
        distanceSlider.addTarget(self, action: #selector(distanceDidChange(slider:)), for: .valueChanged)
        distanceDidChange(slider: distanceSlider)
        
       
    }
    
    
    
    
    @objc func distanceDidChange(slider: UISlider) {
        let currentDistance = Int(slider.value)
        distanceValue.text = "\(currentDistance) Km"
    }

    
    @objc func sizeDidChange(slider: UISlider) {
        if slider.value == 0 {
            sizeValue.text = "smol"
        } else if slider.value == 1 {
            sizeValue.text = "medium"
        } else if slider.value == 2 {
            sizeValue.text = "LARGE"
        }
    }
    
    
  
    
    func sfondobianco(bottone: UIView, scritta: UIButton){
        bottone.backgroundColor = UIColor.white
        scritta.setTitleColor(.orange, for: .normal)
    }
    
    func sfondoclear(bottone: UIView, scritta: UIButton){
        bottone.backgroundColor = UIColor.clear
        scritta.setTitleColor(.white, for: .normal)
    }
    
    
    
    func activateDogButton(image: UIImageView, view: UIView, butn: UIButton){
        valoreboolDog = !valoreboolDog
        if valoreboolDog == true {
            image.image = #imageLiteral(resourceName: "Vector44")
            sfondobianco(bottone: view, scritta: butn)
            
        } else {
            image.image = #imageLiteral(resourceName: "Vector4")
            sfondoclear(bottone: view, scritta: butn)
        }}
    
    func activateCatButton(image: UIImageView, view: UIView, butn: UIButton){
        valoreboolCat = !valoreboolCat
        if valoreboolCat == true {
            image.image = #imageLiteral(resourceName: "Vector55")
            sfondobianco(bottone: view, scritta: butn)
            
        } else {
            image.image = #imageLiteral(resourceName: "Vector5")
            sfondoclear(bottone: view, scritta: butn)
        }}
    
    func activateHeartButton(image: UIImageView, view: UIView, butn: UIButton){
        valoreboolHeart = !valoreboolHeart
        if valoreboolDog == true {
            image.image = #imageLiteral(resourceName: "eart")
            sfondobianco(bottone: view, scritta: butn)
            
        } else {
            image.image = #imageLiteral(resourceName: "heart")
            sfondoclear(bottone: view, scritta: butn)
        }}
    
   
    
    
    
    
    
    
    
    
    
    
    
}
