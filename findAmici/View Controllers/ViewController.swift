//
//  ViewController.swift
//  PierDiego
//
//  Created by Diego Riccardi on 07/05/2020.
//  Copyright © 2020 Diego Riccardi. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate,UITableViewDelegate,UITableViewDataSource {
    
    
    

    
    
    
    @IBOutlet weak var serchBar: UISearchBar!
    
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    var filtri: [Filters] = []
    
    var pets: [PetType] = []
    
    let cellSpacingHeight: CGFloat = 5
    
    
    
    @IBOutlet weak var petListTableView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        self.view.backgroundColor = UIColor(patternImage: (#imageLiteral(resourceName: "orange")))
        
        
        self.petListTableView.dataSource = self
        self.petListTableView.delegate = self
        
        
        
        
        self.pets = [PetType.init(nome: "Toby", image: #imageLiteral(resourceName: "Toby"), gender: #imageLiteral(resourceName: "maschio"), breed: "Scottish Straight", age: "4 months", localitis: "Naples, Italy (0,5 km)",imagess: tobyArr, genderr: "Male"), PetType.init(nome: "Lilly", image: #imageLiteral(resourceName: "Lilly"), gender: #imageLiteral(resourceName: "femmina"), breed: "Akita Inu", age: "2 y.o.", localitis: "Naples, Italy (2 km)", imagess:  lillyArr, genderr: "Female"), PetType.init(nome: "Bowy", image: #imageLiteral(resourceName: "Bowy"), gender: #imageLiteral(resourceName: "maschio"), breed: "Half-Breed", age: "2 months", localitis: "Naples, Italy (5 km)", imagess: bowyArr, genderr: "Male"), PetType.init(nome: "Jackie", image: #imageLiteral(resourceName: "Jackie"), gender: #imageLiteral(resourceName: "maschio"), breed: "Half-Breed", age: "3 months", localitis: "Naples, Italy (2 km)",imagess: jackyArr, genderr: "Male"), PetType.init(nome: "Luna", image: #imageLiteral(resourceName: "Luna"), gender: #imageLiteral(resourceName: "femmina"), breed: "Half-Breed", age: "3 y.o.", localitis: "Naples, Italy (3 km)",imagess: lunaArr, genderr: "Female")]
        
        
        self.filtri = [Filters.init(pic: #imageLiteral(resourceName: "Vector1"), titolo: "FILTERS", identif: 1), Filters.init(pic: #imageLiteral(resourceName: "Vector2"), titolo: "SHELTER", identif: 2), Filters.init(pic: #imageLiteral(resourceName: "Vector3"), titolo: "PRIVATE", identif: 3), Filters.init(pic: #imageLiteral(resourceName: "Vector5"), titolo: "CATS", identif: 4), Filters.init(pic: #imageLiteral(resourceName: "Vector4"), titolo: "DOGS", identif: 5), Filters.init(pic: #imageLiteral(resourceName: "Vector6"), titolo: "MALE", identif: 6) , Filters.init(pic: #imageLiteral(resourceName: "Vector7"), titolo: "FEMALE", identif: 7)]
        
        serchBar.backgroundColor = .clear
        petListTableView.backgroundColor = .clear
        petListTableView.separatorStyle = .none
    }

    let tobyArr:[UIImage] = [#imageLiteral(resourceName: "Toby"),#imageLiteral(resourceName: "Toby copy 3"),#imageLiteral(resourceName: "Toby copy 2")]
    let bowyArr: [UIImage] = [#imageLiteral(resourceName: "Bowy copy 3"),#imageLiteral(resourceName: "Bowy copy 2"),#imageLiteral(resourceName: "Bowy copy")]
    let jackyArr :[UIImage] = [#imageLiteral(resourceName: "Jackie copy 2"),#imageLiteral(resourceName: "Jackie copy"),#imageLiteral(resourceName: "Jackie copy")]
    let lillyArr: [UIImage] = [#imageLiteral(resourceName: "Lilly copy 2"),#imageLiteral(resourceName: "Lilly copy 3"),#imageLiteral(resourceName: "Lilly copy")]
    let lunaArr: [UIImage] = [#imageLiteral(resourceName: "Luna"),#imageLiteral(resourceName: "Luna copy")]
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return filtri.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let filtro = self.filtri[indexPath.row]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! FiltersCollectionViewCell
        
        cell.filterImage.image = filtro.pic
        cell.filterLabel.text = filtro.titolo
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let selectedData = filtri[indexPath.row]
        print(selectedData)
        if selectedData.titolo == "FILTERS" {
            performSegue(withIdentifier: "segue", sender: selectedData)
        
            
        }else if let cell = collectionView.cellForItem(at: indexPath) as? FiltersCollectionViewCell{
            cell.showIcon(sendr: cell)
        }
    }
   
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        if let cell = collectionView.cellForItem(at: indexPath) as? FiltersCollectionViewCell{
            cell.hideIcon(sendr: cell)
        }
        
    }
    
    
    
    
    
    
      func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pets.count
      }
      
      func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let pet = self.pets[indexPath.row]
        let cell: PetListTableViewCell = tableView.dequeueReusableCell(withIdentifier: "cellPet", for: indexPath) as! PetListTableViewCell
        cell.petPic.image = pet.image
        cell.petName.text = pet.nome
        cell.petAge.text = pet.age
        cell.petBreed.text = pet.breed
        cell.localita.text = pet.localitis
        cell.petGender.image = pet.gender
        
        return cell
      }

    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 135
        
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return cellSpacingHeight
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    
    
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let petSelected = self.pets[indexPath.row]
        self.performSegue(withIdentifier: "detailsegue", sender: petSelected)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.destination is DetailViewController {
            let vc = segue.destination as! DetailViewController
            vc.recivedPet = sender as? PetType
    }
    }

    
    
}

