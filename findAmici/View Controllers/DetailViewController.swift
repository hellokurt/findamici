//
//  DetailViewController.swift
//  PetSociety
//
//  Created by Diego Riccardi on 27/05/2020.
//  Copyright © 2020 Diego Riccardi. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

    
    
    
    struct Fotos {
       var imagine:UIImage
    }
    

    
    @IBOutlet weak var nomePet: UILabel!
    
    @IBOutlet weak var localitaa: UILabel!
    
    @IBOutlet weak var sexPet: UILabel!
    
    @IBOutlet weak var breedPet: UILabel!
    
    @IBOutlet weak var agePet: UILabel!
    
    @IBOutlet weak var bota: UIView!
    @IBOutlet weak var botb: UIView!
    @IBOutlet weak var botc: UIView!
    
    @IBOutlet weak var heartButton: UIButton!
    @IBOutlet weak var secondHeart: UIButton!
    
    @IBAction func heartTapped(_ sender: Any) {
        if secondHeart.isHidden == true {
            secondHeart.isHidden = false
        } else{
            secondHeart.isHidden = true
        }    }
    
    @IBAction func secondTapped(_ sender: Any) {
        
    }
    
    @IBOutlet weak var imageviu: UIImageView!
    
    @IBOutlet weak var detailCollectionView: UICollectionView!
    
    var recivedPet :PetType?
    var pics : [Fotos] = []
    var cazzo : [UIImage] = []
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        secondHeart.isHidden = true

        bota.layer.borderWidth = 2
        bota.layer.borderColor = UIColor.orange.cgColor
        bota.layer.cornerRadius = 18
        
        
        botb.layer.borderWidth = 2
        botb.layer.borderColor = UIColor.orange.cgColor
        botb.layer.cornerRadius = 18
              
        botc.layer.borderWidth = 2
        botc.layer.borderColor = UIColor.orange.cgColor
        botc.layer.cornerRadius = 18
              
        self.nomePet.text = self.recivedPet?.nome
        self.localitaa.text = self.recivedPet?.localitis
        self.breedPet.text = self.recivedPet?.breed
        self.sexPet.text = self.recivedPet?.genderr
        self.agePet.text = self.recivedPet?.age
        self.imageviu.image = self.recivedPet?.image
        self.cazzo = recivedPet!.imagess
        
        if recivedPet?.nome == "Toby"{
            self.actualArray = tobyArr
        } else if recivedPet?.nome == "Bowy"{
            self.actualArray = bowyArr
        }else if recivedPet?.nome == "Lilly"{
            self.actualArray = lillyArr
        }else if recivedPet?.nome == "Jackie"{
            self.actualArray = jackyArr
        }else if recivedPet?.nome == "Luna"{
            self.actualArray = lunaArr
        }
    
        
        
      
    }
    
    let tobyArr:[UIImage] = [#imageLiteral(resourceName: "Toby-1"),#imageLiteral(resourceName: "Toby copy 3"),#imageLiteral(resourceName: "Toby copy 2")]
     let bowyArr: [UIImage] = [#imageLiteral(resourceName: "Bowy copy 3"),#imageLiteral(resourceName: "Bowy copy 2"),#imageLiteral(resourceName: "Bowy copy")]
     let jackyArr :[UIImage] = [#imageLiteral(resourceName: "Jackie copy 2"),#imageLiteral(resourceName: "Jackie copy"),#imageLiteral(resourceName: "Jackie copy")]
     let lillyArr: [UIImage] = [#imageLiteral(resourceName: "Lilly copy 2"),#imageLiteral(resourceName: "Lilly copy 3"),#imageLiteral(resourceName: "Lilly copy")]
     let lunaArr: [UIImage] = [#imageLiteral(resourceName: "Luna"),#imageLiteral(resourceName: "Luna copy")]
    
    var actualArray:[UIImage] = []
    
//    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        return actualArray.count
//       }
//
//       func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//        let foto = self.actualArray[indexPath.row]
//        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellPet", for: indexPath) as! DetaiklCollectionViewCell
//
//        cell.imageView.image = foto
//
//        return cell
//       }
    
    
    

}
