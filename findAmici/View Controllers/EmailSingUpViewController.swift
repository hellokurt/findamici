//
//  EmailSingUpViewController.swift
//  PierDiego
//
//  Created by Diego Riccardi on 26/05/2020.
//  Copyright © 2020 Diego Riccardi. All rights reserved.
//

import UIKit

class EmailSingUpViewController: UIViewController {

    @IBOutlet weak var firstNameTextField: UITextField!
    
    @IBOutlet weak var lastNameTextField: UITextField!
    
    @IBOutlet weak var emailTextField: UITextField!
    
    @IBOutlet weak var passwordTextField: UITextField!
    
    @IBOutlet weak var phoneNumberTextField: UITextField!
    
    
    
    
    @IBOutlet weak var singUpButton: UIButton!
    
    
    
    @IBAction func singUpTapped(_ sender: Any) {
        
        
//        validate the fields
        
        
//        creating the user
        
        
//        transition to the homescreen
        
        
    }
    
    
    
//     check the fields and validate that the data is correct. If everything is correct, this methoid returns nil. Otherwise, it returns the rror message
    
    func validateFields() -> String? {
        
//        check that all fields are filled in
        
        if firstNameTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" || lastNameTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" || emailTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" || passwordTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" {
            
            
            
            return "Please fill in all fields."
        }
        
        
        
        
        return nil
    }
    
    
    static func isPasswordValid(_ password: String) -> Bool {
        let passwordTest = NSPredicate(format: "SELF MATCHES %@", "^(?=.*[a-z]){6,}")
        return passwordTest.evaluate(with: password)
    }
    
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        singUpButton.layer.borderColor = #colorLiteral(red: 1, green: 0.4324012399, blue: 0.3889559209, alpha: 1)
        singUpButton.layer.borderWidth = 2
        singUpButton.layer.cornerRadius = 23
        
        

    }
    

   
}
