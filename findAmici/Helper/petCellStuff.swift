//
//  petCellStuff.swift
//  PetSociety
//
//  Created by Diego Riccardi on 26/05/2020.
//  Copyright © 2020 Diego Riccardi. All rights reserved.
//

import Foundation
import UIKit

struct PetType {
    let nome : String
    let image : UIImage
    let gender : UIImage
    let breed: String
    let age: String
    let localitis: String
    let imagess: [UIImage]
    let genderr: String
}
